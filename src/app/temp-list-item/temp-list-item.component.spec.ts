import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TempListItemComponent } from './temp-list-item.component';

describe('TempListItemComponent', () => {
  let component: TempListItemComponent;
  let fixture: ComponentFixture<TempListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TempListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TempListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
