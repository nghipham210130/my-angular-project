import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ItemService } from '../item.service';
import { Item } from '../item.model'

@Component({
  selector: 'app-temp-list-item',
  templateUrl: './temp-list-item.component.html',
  styleUrls: ['./temp-list-item.component.scss']
})
export class TempListItemComponent implements OnInit {
  @Input() item: Item;
  @Output() counter = new EventEmitter<number>();;
  items: Item[];
  constructor(private service: ItemService) { }

  ngOnInit() {
  }

  onDelete(id: number) {
    this.service.deleteItem(id);
    this.counter.emit(this.service.getItemsByList(this.item.listId).length);

}

onCheck(){
  if(this.item.status == true){    
    return true;
  }
  else{
    return false;
  } 
}

onChange(){
  if(this.item.status == true){    
    return this.item.status = false;
  }
  else{
    return this.item.status = true;
  } 
}

}
