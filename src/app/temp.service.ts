import { Injectable } from '@angular/core';
import {Temp} from './temp.model';

@Injectable({
  providedIn: 'root'
})
export class TempService {
  temps: Temp[] = [
    {id: 1, title: 'The Best Recommend', status: "Best" },
    {id: 2, title: 'Recommend For Busy Days', status: "Busy" },
    {id: 3, title: 'Recommend For Relax Days', status: "Relax" },
    {id: 4, title: 'Recommend For Normal Days', status: "Normal" },
  ];

  constructor() { }

  getTempsList(){
    return this.temps;
  }

  getTempById(id: number | string) {
    return this.temps[this.temps.findIndex(x => x.id === id)];
  }

  addNewTemp (newTemp){
    this.temps.push(newTemp);
    console.log(this.temps);
  }

  deleteTemp (id){
    let index = this.temps.findIndex(d => d.id === id); //find index in your array
    this.temps.splice(index, 1);//remove element from array
  }
}
