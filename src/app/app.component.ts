import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ex1-npham1';
  logoUrl = '../assets/img/Capgemini_Logo.png';
}
