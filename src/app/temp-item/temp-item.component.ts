import { Component, OnInit, Input } from '@angular/core';
import {TempService} from '../temp.service';

@Component({
  selector: 'app-temp-item',
  templateUrl: './temp-item.component.html',
  styleUrls: ['./temp-item.component.scss']
})
export class TempItemComponent implements OnInit {
  @Input() temp;
  

  constructor( private service: TempService) { }

  ngOnInit() {
  }

  onDelete(id: number) {
    this.service.deleteTemp(id);
}

}
