export class Item {
    id: number;
    title: string;
    listId: number;
    status: boolean;
}