import { Component, OnInit } from '@angular/core';
import {TempService} from '../temp.service';
import {FormBuilder, ReactiveFormsModule} from '@angular/forms';
import { Temp} from '../temp.model';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-temp-list',
  templateUrl: './temp-list.component.html',
  styleUrls: ['./temp-list.component.scss'],
  providers: [NgbModal]
})
export class TempListComponent implements OnInit {
  temps: Temp[];
  checkoutForm;
  closeResult: string;

  constructor( 
    private modalService: NgbModal,
    private formBuilder: FormBuilder, 
    private service: TempService,) {
      this.checkoutForm = this.formBuilder.group({
        title: '',
        status: ''
      });
     }

  ngOnInit() {
    this.getTemps();
  }

  getTemps(){
    this.temps = this.service.getTempsList();
  }

  onSubmit(data){
    const tempAdded: Temp = {
      id: this.service.temps.length + 1,
      title: data.title.value,
      status: data.status.value,
    }
    //console.log(data.title);
    this.service.addNewTemp(tempAdded);
    this.modalService.dismissAll();
     }

   open(content) {
    this.modalService.open(content);
  }

}
