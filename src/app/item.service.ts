import { Injectable } from '@angular/core';
import {Item} from './item.model'
import { identifierModuleUrl } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  items: Item[] = [
    {id: 1, title: 'Exercise', listId: 1, status: true},
    {id: 2, title: 'Go shopping', listId: 1, status: false},
    {id: 3, title: 'Do homework', listId: 2, status: false},
    {id: 4, title: 'Buy coffe', listId: 4, status: false},
    {id: 5, title: 'Repair', listId: 2, status: false},
    {id: 6, title: 'Read book', listId: 1, status: false},
    {id: 7, title: 'Listen to music', listId: 4, status: false},
    {id: 8, title: 'Cook a meal', listId: 1, status: false},
    {id: 9, title: 'Run', listId: 1, status: false},
    {id: 10, title: 'Walk', listId: 3, status: false},
    {id: 11, title: 'Talk with Mi', listId: 3, status: false},
    {id: 12, title: 'Study something', listId: 1, status: false},
    {id: 13, title: 'Learn English', listId: 4, status: false},
    {id: 14, title: 'Do exercises', listId: 2, status: false},
    {id: 15, title: 'Clean the house', listId: 2, status: false},

  ];

  constructor() { }

  getItemsList(){
    return this.items;
  }

  getItemsByList(listId){
    return this.items.filter(x => x.listId === listId);
  }

  getItemById(id: number | string ){
    return this.items[this.items.findIndex(x => (x.id === id))];
  }

  addNewItem (newItem){
    this.items.push(newItem);
    console.log(this.items);
  }

  deleteItem (id){
    let index = this.items.findIndex(d => d.id === id); //find index in your array
    this.items.splice(index, 1);//remove element from array
  }
}
