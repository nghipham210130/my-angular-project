import { Component, OnInit} from '@angular/core';
import { TempService} from '../temp.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Temp} from '../temp.model';
import { Item} from '../item.model';
import { ItemService } from '../item.service';

@Component({
  selector: 'app-temp-detail',
  templateUrl: './temp-detail.component.html',
  styleUrls: ['./temp-detail.component.scss']
})
export class TempDetailComponent implements OnInit {
  items: Item[];
  temp: Temp;
  countRow: number;
  maxID: number = 0;

  constructor(private serviceTemp: TempService, 
    private serviceItem: ItemService,
    private route: ActivatedRoute,) {}

  ngOnInit() {
    console.log(this.route);
    
    this.route.params.subscribe(params => {
      this.temp = this.serviceTemp.getTempById(+params["id"]);
      this.countRow = this.serviceItem.getItemsByList(this.temp.id).length;
    });

    this.getItems();
  }

  getItems(){
    this.items = this.serviceItem.getItemsList();
  }

  getMaxID(){
    for (let index = 0; index < this.items.length; index++) {
      if(this.items[index].id > this.maxID)  this.maxID = this.items[index].id;             
    }
    return this.maxID;
  }


  onAdd(new_Item: string){
    console.log(typeof(new_Item));
    console.log(new_Item);
    if(new_Item === null || new_Item === '')
    {
      alert("Null item");        
    }     
    else{
      const itemAdded: Item = {
        id: this.getMaxID() + 1,      
        title: new_Item,
        listId: this.temp.id,
        status: false,
      }
      this.serviceItem.addNewItem(itemAdded);
      this.countRow++;   
    }
  }

  onDeleteUpdateCount(counter){
    this.countRow = counter;
  }
}
