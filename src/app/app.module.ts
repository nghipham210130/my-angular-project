
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';
//import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TempListComponent } from './temp-list/temp-list.component';
import { TempItemComponent } from './temp-item/temp-item.component';
import { TempDetailComponent} from './temp-detail/temp-detail.component';
import { TempService } from './temp.service';
import { Routes, RouterModule } from '@angular/router';
import { TempListItemComponent } from './temp-list-item/temp-list-item.component';

const appRoutes: Routes = [
  { path: '', component: TempListComponent },
  { path: 'temps', component: TempListComponent },
  { path: 'temps/:id', component: TempDetailComponent},
  

]

@NgModule({
  declarations: [
    AppComponent,
    TempListComponent,
    TempItemComponent,
    TempDetailComponent,
    TempListItemComponent,

  ],
  imports: [
    BrowserModule,
    NgbModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [TempService],
  bootstrap: [AppComponent],
  entryComponents: [TempListComponent]
})
export class AppModule { }
